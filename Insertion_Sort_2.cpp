#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define TAM 1000

void fInsertion_Sort(int *pVetor);

int main()
{
    int vVetor[TAM];
    int vAux;
    int nt=0;

    srand(time(NULL));
    for (vAux=0; vAux < TAM; vAux++)
    {
        vVetor[vAux] = (rand() % 90) + 10; 
        printf(" %d,",vVetor[vAux]);
    }

    fInsertion_Sort(vVetor); 

    printf("\n\n");
    for (vAux=0; vAux < TAM; vAux++)
    {
        printf(" %d,",vVetor[vAux]);
    }

	printf("%i",nt);
    printf("\n\n ");
    system("pause");
}

void fInsertion_Sort(int *pVetor)
{
    int vAux;
    int vTemp;
    int vTroca;
    int nt=0;

    for (vAux=1; vAux < TAM; vAux++)
    {
        vTemp = vAux; 

        while (pVetor[vTemp] < pVetor[vTemp-1]) 
        { 
            vTroca          = pVetor[vTemp];
            pVetor[vTemp]   = pVetor[vTemp-1];
            pVetor[vTemp-1] = vTroca;
            vTemp--; 
            nt++;

            if (vTemp == 0) 
                break;
        }

    }
}
